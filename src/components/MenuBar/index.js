import React, { useState, useEffect } from 'react';
import * as S from './styles';

//components
//import { StickyContainer, Sticky } from 'react-sticky';

//icons
import Videopasal from '../../icons/VideoPasal2.png';
import VideopasalM from '../../icons/VideoPasal3.png';

function MenuBar({ className, pose, initialPose, selected, setScreen, onClick, mainIcon }) {
  const [active, setActive] = useState(false);

  const logSizePosition = e => {
    if (window.innerWidth  > 600) {
      setActive(false);
    } else {
      setActive(true);
    }
  };

  useEffect(() => {
    window.addEventListener('load', logSizePosition);
    window.addEventListener('resize', logSizePosition);
    return () => {
      window.removeEventListener('resize', logSizePosition);
    };
  }, []);

  return (
    <S.MenuBar className={className} pose={pose} initialPose={initialPose}>
      <S.Header tabIndex={0}>
        {active ? (
          <img src={VideopasalM} alt="logo" width="66" height="auto" />
        ) : (
          <img src={Videopasal} alt="logo" width="220" height="auto" />
        )}
      </S.Header>
      <S.Icons>
        <S.Item>
          <S.Link onClick={() => setScreen(screen => 'home')}>Home</S.Link>
          <S.Link onClick={() => setScreen(screen => 'privacy')}>Privacy</S.Link>
          <S.Link onClick={() => setScreen(screen => 'disclamer')}>Disclaimer</S.Link>
          <S.Link onClick={() => setScreen(screen => 'contact')}>Contact</S.Link>
        </S.Item>
        <S.Item
          className="share"
          selected={selected}
          onClick={onClick}
          role="button"
          tabIndex={0}
          onKeyPress={e => {
            if (e.which === 13 || e.which === 32) {
              onClick(e);
            }
          }}
        >
          <S.Icon icon={mainIcon} />
        </S.Item>
      </S.Icons>
    </S.MenuBar>
  );
}

export default MenuBar;
