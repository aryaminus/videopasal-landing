import React, { useRef, useState } from 'react';

//icons (imported as svg using babel plugin)
import faFeather from '../../icons/feather.svg';

//styles
import * as S from './styles';
import * as A from 'styles/shared-components';
// import 'bootstrap/dist/css/bootstrap.css';
// import 'styles/App.css';
// import 'styles/animate.css';
import themes from 'styles/themes';

//components
import { Card, CardText, CardBody, CardTitle, Container, Row, Col, Button } from 'reactstrap';
import MenuBar from 'components/MenuBar';
import Compose from 'components/Compose';
import DownloadButton from 'components/DownloadButton';
import Background from 'components/Background';
import { ThemeProvider } from 'styled-components';
import Privacy from 'components/Static/privacy.js';
import Disclamer from 'components/Static/disclamer.js';
import Contact from 'components/Static/contact.js';

//hooks
import {
  //useGoogleAnalytics,
  useHovered,
  useFindElementCenter,
  useMousePosition,
  useCanHover
} from 'utils/hooks';
import useIntroAnimation from './use-intro-animation';

/**
 * Show outline only on keyboard interaction
 *
 * Adds 'js-focus-visible' class to body and 'focus-visible' class to focused element
 *
 * https://github.com/WICG/focus-visible
 * https://davidwalsh.name/css-focus
 */
import 'focus-visible';

//env
//const { REACT_APP_ANALYTICS_ID } = process.env;

//icons
import Videopasal from '../../icons/VideoPasal4.png';

function Home() {
  const [composeIsOpen, setComposeOpen] = useState(false);
  const [text, setText] = useState(
    `Woah! With videopasal.com I can view all the latest Nepali movies and series 😄️`
  );
  const [screen, setScreen] = useState('home');

  // refs
  const contentRef = useRef();
  const messagesWindowRef = useRef();

  //custom hooks
  const { isAnimationDone, menuBarPose, homePose } = useIntroAnimation();
  const canHover = useCanHover();
  const isHoveringMessages = useHovered();
  const isHoveringCompose = useHovered();
  const windowCenter = useFindElementCenter(messagesWindowRef);
  const { y: mouseY } = useMousePosition(isHoveringCompose.value);

  // side effects
  //useGoogleAnalytics(REACT_APP_ANALYTICS_ID, isAnimationDone);

  // computed
  const isNotHoveringMenuBar = mouseY === null || mouseY >= 25;
  const showComposeWindow = composeIsOpen || (isHoveringCompose.value && isNotHoveringMenuBar);
  const isBig = window.innerWidth > 450;

  return (
    <ThemeProvider theme={themes['dark']}>
      <S.Home>
        <S.MainSection>
          <Background night={true} startLoadingLight={isAnimationDone} show={isBig} />

          <MenuBar
            className="menubar"
            pose={menuBarPose}
            selected={showComposeWindow}
            onClick={() => {
              setComposeOpen(v => !v);
            }}
            setScreen={setScreen}
            mainIcon={faFeather}
          />

          <Compose
            {...isHoveringCompose.bind}
            text={text}
            setText={setText}
            setComposeOpen={setComposeOpen}
            composeIsOpen={composeIsOpen}
            visible={showComposeWindow}
          />

          <S.Content ref={contentRef}>
            <S.WindowBox ref={messagesWindowRef} 
            //initialPose="hidden" 
            //pose={homePose} 
            {...windowCenter}>
              <S.Window night={false} hovering={isHoveringMessages.value} />
            </S.WindowBox>

            <A.Space huge />

            <S.TextContent isAnimationDone={isAnimationDone} pose={homePose}>
              {/* {screen != 'home' ? <S.Title> VideoPasal </S.Title> : null} */}

              <S.Subtitle>
                    {screen == 'home' ? (
                      <div id="Description">
                        <A.Space huge />
                        <center>
                          <img src={Videopasal} alt="logo" width="150" height="auto" />
                        </center>
                        <div>
                          <span>
                            <br />
                            Just <A.Hover {...isHoveringMessages.bind}>download</A.Hover> the app or{' '}
                            <A.Hover
                              {...(canHover
                                ? isHoveringCompose.bind
                                : { onClick: () => setComposeOpen(true) })}
                              className="tweeting"
                            >
                              share
                            </A.Hover>{' '}
                            about the app. Your one-to-go solution to get the latest Nepali content worldwide.
                          </span>
                        </div>
                        {/* <span>Your one-to-go solution to get the latest Nepali content worldwide.</span>{' '} */}
                      </div>
                    ) : null}
                    {screen == 'contact' ? (
                      <div>
                        <h3>Satyawati LLC</h3>
                        <span>1401 Esters Road, Irving, Texas 75061, United States</span>
                        <br />
                        <a href="tel:469-233-6452"> Tel: 469-233-6452</a>
                        <A.Space medium />
                      </div>
                    ) : null}
                    {screen == 'privacy' ? <Privacy /> : null}
                    {screen == 'disclamer' ? <Disclamer /> : null}
                    {screen == 'contact' ? <Contact /> : null}
              </S.Subtitle>

              <A.Space />

              {screen != 'contact' ? <DownloadButton startLoading={isAnimationDone} /> : null}

              <A.Space />
            </S.TextContent>
          </S.Content>
        </S.MainSection>
        <S.Footer initialPose="hidden" pose={composeIsOpen ? 'invisible' : menuBarPose}>
          <S.Links>
            <S.Link href="mailto:contact@videopasal.com">Mail Us</S.Link>
            <S.Link onClick={() => setScreen(screen => 'home')}>Home</S.Link>
            <S.Link onClick={() => setScreen(screen => 'privacy')}>Privacy</S.Link>
            <S.Link onClick={() => setScreen(screen => 'disclamer')}>Disclaimer</S.Link>
            <S.Link onClick={() => setScreen(screen => 'contact')}>Contact us</S.Link>
          </S.Links>
        </S.Footer>
      </S.Home>
    </ThemeProvider>
  );
}

export default Home;
