import styled from 'styled-components';
import flex from 'styles/flex';
import { applyTheme, getThemeColor, hover, when } from 'styles/mixins';

import { isHorizontal } from 'styles/responsive';

//components
import Icon from 'icons/Icon';

export const Input = styled.textarea(
    {
      borderRadius: 3,
      padding: 10,
      border: 'none',
      outline: 'none',
      minHeight: 30,
      width: '100%',
      resize: 'none',
      borderBottom: '2px solid #FC5116'
    },
    applyTheme('contactInput'),
    when('disabled', {
      borderBottom: '2px solid #e0245d'
    })
  );

  export const InputMessage = styled.textarea(
    {
      borderRadius: 3,
      padding: 10,
      border: 'none',
      outline: 'none',
      minHeight: 120,
      width: '100%',
      resize: 'none',
      borderBottom: '2px solid #FC5116'
    },
    applyTheme('contactInput'),
    when('disabled', {
      borderBottom: '2px solid #e0245d'
    })
  );

export const Button = styled.button(
  {
    ...flex.horizontal,
    ...flex.centerHorizontal,
    userSelect: 'none',
    border: 'none',
    fontSize: 18,
    borderRadius: 4,
    maxWidth: 140,
    padding: 15,
    width: '100%',
    transition: 'all 150ms linear',
    boxShadow: '0px 1px 1px rgba(0, 0, 0, 0.2)'
  },
  applyTheme('button'),
  when('disabled', {
    opacity: 0.5,
    cursor: 'not-allowed'
  }),
  ({ theme }) => ({
    ...(!theme.disabled && {
      ...hover({
        background: theme.button.backgroundHover
      })
    })
  })
);

export const AIcon = styled(Icon)(
  {
    marginRight: 15,
    width: 20,
    height: 20,
    position: 'relative',
    top: -1
  },
  getThemeColor('text', 'fill')
);

export const Content = styled.div({
  //...flex.horizontal,
  // ...zIndexFor(ELEMENTS.CONTENT),
  marginTop: 'auto',
  marginBottom: 'auto',
  [isHorizontal]: {
    flexDirection: 'column-reverse',
    ...flex.centerVertical
  }
});

export const NameEmail = styled.div({
    ...flex.horizontal,
    // ...zIndexFor(ELEMENTS.CONTENT),
    marginTop: 'auto',
    marginBottom: 'auto',
    [isHorizontal]: {
      flexDirection: 'column-reverse',
      ...flex.centerVertical
    }
  });
