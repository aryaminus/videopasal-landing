export default {
  dark: {
    name: 'dark',
    messages: {
      backgroundColor: '#1a2837'
    },
    bar: {
      backgroundColor: '#233448',
      borderBottom: '1px solid rgba(0,0,0,0)'
    },
    messageWrap: {
      borderBottom: '1px solid rgba(0,0,0,0.5)'
    },
    windowBar: {
      backgroundColor: '#16202d'
    },
    colors: {
      dimmed: '#8799a7',
      icon: '#72bfff',
      text: 'white'
    },
    button: {
      background: '#FC5116',
      backgroundHover: '#EB0020',
      color: '#fff',
      opacity: 0.7
    },
    composeInput: {
      backgroundColor: '#192530',
      color: 'white'
    },
    contactInput: {
      backgroundColor: 'rgba(0, 0, 0, 0.2)',
      color: 'white'
    },
    composeWindow: {
      backgroundColor: '#1a2836'
    },
    composeBar: {
      backgroundColor: '#243547',
      borderBottom: '1px solid rgba(0,0,0,0)'
    }
  }
};
