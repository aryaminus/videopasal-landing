import React from 'react';

//styles
import * as S from './styles';

function Privacy({}) {
  return (
    <S.Content>
      <div id="Description">
        <h3>Privacy Policy</h3>
        <p>
          We take your privacy very seriously. This policy describes all information collected or submitted on
          the VideoPasal application for App Store and Play Store, and what we do with it.
        </p>
        <h3>Data Controllers and Owners</h3>
        <p>SatyaTech & OSR</p>
        <h3>Questions</h3>
        <p>
          If you have questions about deleting or correcting your personal data please contact us at
          contact@VideoPasal.com
        </p>
        <h3>Protection of Information</h3>
        <p>No Personally-Identifying Information is transmitted to our servers when using VideoPasal.</p>
        <p>
          We collect anonymous usage data and crash reports . This makes it easier for us to troubleshoot
          problems and improve VideoPasal. Hence Usage Data is collected only to operate and improve the app
          and customer support.
        </p>
        <h3>Security</h3>
        <p>
          The security of your personal data also depends on you. If you believe or find out that your login
          credentials are in the wrong hands, you should immediately change your password.We are not
          responsible for such unauthorized access.
        </p>
        <h3>Changes to this policy</h3>
        <p>
          Any future change to this policy will be posted to this page. Your continued use of the VideoPasal
          app after we make any of these changes is deemed to be in acceptance of those changes, so please
          check this page periodically for updates.
        </p>
        <h3>Your Consent</h3>
        <p>By using our app, you consent to our privacy policy.</p>
        <p>If you have questions, you can reach us at contact@VideoPasal.com</p>
      </div>
    </S.Content>
  );
}

export default Privacy;
