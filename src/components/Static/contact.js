import React, { useRef, useState } from 'react';

//components
import axios from 'axios';

//styles
import * as S from './styles';
import * as A from 'styles/shared-components';

//icons
import faSend from '../../icons/send.svg';

function Contact({}) {
  const [name, setName] = useState(``);
  const [email, setEmail] = useState(``);
  const [subject, setSubject] = useState(``);
  const [message, setMessage] = useState(``);
  const [error, setError] = useState(``);
  const [output, setOutput] = useState(false);
  const [server, setServer] = useState(0);

  const checkFormErrors = async (name, email, message, subject) => {
    if (name.length < 2) {
      setOutput(output => false);
      setError('Name needs to be at least 2 characters.');
    } else if (email.length < 10) {
      setOutput(output => false);
      setError('Email needs to be at least 10 characters.');
    } else if (subject.length < 5) {
      setOutput(output => false);
      setError('Subject needs to be at least 5 characters.');
    } else if (message.length < 5) {
      setOutput(output => false);
      setError('Message needs to be at least 5 characters.');
    } else {
      setOutput(output => true);
    }
  };

  const send = async () => {
    const url = 'https://satyatech.xyz:4000/video-pasal-support';
    const data = {
      name: name,
      email: email,
      query: message,
      subject: subject
    };
    checkFormErrors(name, email, message, subject);
    if (output == true) {
      axios.post(url, data).then(result => {
        console.log(result);
        setServer(server => result.data.success);
        //setOutput(output => true);
      });
    }
  };

  return (
    <S.Content>
      <S.NameEmail>
        <S.Input
          placeholder="Enter your Name"
          autoComplete="off"
          autoCorrect="off"
          autoCapitalize="off"
          spellCheck={false}
          //value={name}
          onChange={e => setName(e.target.value)}
        />
        <A.Space small />
        <S.Input
          placeholder="Enter your Email"
          autoComplete="off"
          autoCorrect="off"
          autoCapitalize="off"
          spellCheck={false}
          //value={email}
          onChange={e => setEmail(e.target.value)}
        />
      </S.NameEmail>

      <A.Space small />
      <S.Input
        placeholder="Enter the Subject"
        autoComplete="off"
        autoCorrect="off"
        autoCapitalize="off"
        spellCheck={false}
        //value={subject}
        onChange={e => setSubject(e.target.value)}
      />

      <A.Space small />
      <S.InputMessage
        placeholder="Enter the Message"
        autoComplete="off"
        autoCorrect="off"
        autoCapitalize="off"
        spellCheck={false}
        //value={message}
        onChange={e => setMessage(e.target.value)}
      />

      <center>
      <A.Space small />
        <S.Button
          onClick={send}
          role="button"
          tabIndex={0}
          onKeyPress={e => {
            if ((e.which === 13 || e.which === 32) && ready) {
              send(e);
            }
          }}
        >
          <S.AIcon icon={faSend} />
          <span>Send</span>
        </S.Button>
      </center>
      {server == 1 ? (
        <center>
          <p>Message sent sucessfully!</p>
        </center>
      ) : null}
      {output ? null : (
        <center>
          <p>{error}</p>
        </center>
      )}
      <A.Space small />
    </S.Content>
  );
}

export default Contact;
