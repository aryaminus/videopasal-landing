import React, { Fragment, useState } from 'react';

//styles
import * as S from './styles';

//images
import desertDark from 'images/desert-dark.svg';

function Background({ show, night }) {
  const [desertLoaded, setDesertLoad] = useState(false);

  return (
    show && (
      <Fragment>
        <S.Desert show={night && desertLoaded} onLoad={() => setDesertLoad(true)} src={desertDark} />
      </Fragment>
    )
  );
}

export default Background;
