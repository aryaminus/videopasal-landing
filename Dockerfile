FROM node:latest
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY . .
RUN ls
RUN yarn
RUN yarn build
EXPOSE 80
CMD ["npm", "start"]