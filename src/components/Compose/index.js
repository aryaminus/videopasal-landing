import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';

//styles
import * as S from './styles';
import * as A from 'styles/shared-components';

//components
import { PoseGroup } from 'react-pose';

function Compose({ visible, text, onMouseLeave, onMouseOver, setText, composeIsOpen, setComposeOpen }) {
  const tweet = () => window.open(`http://twitter.com/share?text=${text}&url=https://videopasal.com`);
  const fbshare = () => window.open(`https://www.facebook.com/sharer.php?u=https%3A%2F%2Fvideopasal.com%2F`);
  const remaining = 240 - text.length;
  const overLimit = remaining < 0;

  const compose = (
    <S.Compose onMouseOver={onMouseOver} onMouseLeave={onMouseLeave} visible={visible} className="compose">
      <S.Bar>
        <S.Tweet
          disabled={text.trim() === '' || overLimit || !visible}
          onClick={tweet}
          {...(visible
            ? {
                role: 'button',
                tabIndex: 0,
                onKeyPress: e => {
                  if (e.which === 13 || e.which === 32) {
                    tweet(e);
                  }
                }
              }
            : {})}
        >
          Tweet
        </S.Tweet>

        <A.Space small />

        <S.Facebook
          disabled={text.trim() === '' || overLimit || !visible}
          onClick={fbshare}
          {...(visible
            ? {
                role: 'button',
                tabIndex: 0,
                onKeyPress: e => {
                  if (e.which === 13 || e.which === 32) {
                    fbshare(e);
                  }
                }
              }
            : {})}
        >
          Facebook
        </S.Facebook>
      </S.Bar>
      <S.Content>
        <S.Input
          placeholder="What's happening?"
          overLimit={overLimit}
          autoComplete="off"
          autoCorrect="off"
          autoCapitalize="off"
          spellCheck={false}
          value={text}
          onChange={e => setText(e.target.value)}
        />
      </S.Content>
    </S.Compose>
  );

  let content = (
    <Fragment>
      <PoseGroup>
        {composeIsOpen && (
          <S.Overlay
            key="overlay"
            onClick={() => setComposeOpen(false)}
            {...(visible
              ? {
                  role: 'button',
                  tabIndex: 0,
                  onKeyPress: e => {
                    if (e.which === 13 || e.which === 32) {
                      setComposeOpen(false);
                    }
                  }
                }
              : {})}
          />
        )}
      </PoseGroup>
      {compose}
    </Fragment>
  );

  return ReactDOM.createPortal(content, document.getElementById('compose'));
}

export default Compose;
