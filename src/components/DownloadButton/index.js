import React from 'react';

//import { LoadScript } from 'components/Script';
import * as S from './styles';
import * as A from 'styles/shared-components';

//icons
import faApple from '../../icons/apple.svg';
import faAndroid from '../../icons/android.svg';
import { isDev } from 'utils/dev-prod';

//component
import { isAndroid, isIOS, isBrowser } from 'react-device-detect';

//env
//const { REACT_APP_PADDLE_VENDOR, REACT_APP_PADDLE_PRODUCT_ID } = process.env;

function DownloadButton({ startLoading }) {
  const download = async () => {
    if (isIOS) {
      window.open(`itms-apps://itunes.apple.com/us/app/videopasal/id1442963499`);
    } else {
      window.open(`https://itunes.apple.com/us/app/videopasal/id1442963499?mt=8&uo=4&open=true`);
    }
  };

  const download2 = async () => {
    if (isAndroid) {
      window.open(`market://details?id=xyz.satyatech.VideoPasal`);
    } else {
      window.open(`https://play.google.com/store/apps/details?id=xyz.satyatech.VideoPasal`);
    }
  };

  return (
    <S.Content>
      <S.Button1
        onClick={download}
        role="button"
        tabIndex={0}
        onKeyPress={e => {
          if ((e.which === 13 || e.which === 32) && ready) {
            download(e);
          }
        }}
      >
        <S.AppleIcon icon={faApple} />
        <span>Download iOS</span>
      </S.Button1>

      <A.Space small />

      <S.Button2
        onClick={download2}
        role="button"
        tabIndex={0}
        onKeyPress={e => {
          if ((e.which === 13 || e.which === 32) && ready) {
            download2(e);
          }
        }}
      >
        <S.AndroidIcon icon={faAndroid} />
        <span>Download Android</span>
      </S.Button2>
    </S.Content>
  );
}

export default DownloadButton;
