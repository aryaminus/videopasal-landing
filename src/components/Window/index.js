import React, { useState, useEffect } from 'react';
import * as S from './styles';

//image
import gif from 'images/VPGif.gif';

function Window({ night, ...rest }) {
  // const [active, setActive] = useState(0);

  // const logScrollPosition = e => {
  //   const { pageYOffset } = window;

  //   if (pageYOffset >= 500 && active === 0) {
  //     setActive(1);
  //   } else if (pageYOffset < 500 && active === 1) {
  //     setActive(0);
  //   }
  // };

  // useEffect(() => {
  //   window.addEventListener('scroll', logScrollPosition);
  //   console.log('Created');
  //   return () => {
  //     console.log('Cleaned up');
  //     window.removeEventListener('scroll', logScrollPosition);
  //   };
  // }, []);

  return (
    <S.Window {...rest}>
    <img src={gif} alt="logo" height="720" width="360" />
    </S.Window>
  );
}

export default Window;
