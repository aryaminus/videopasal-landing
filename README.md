### Made by VideoPasal

---

# VideoPasal

✉️️ Landing page for [VideoPasal](https://videopasal.com)

## 🐟️ Custom hooks

All of the hooks are in utils/hooks.js [utils/hooks.js]

- `useCanHover` - detect if the device supports hover, if it doesn't, use `onClick` for the "tweeting and sharing" button
- `useMousePosition` - track the mouse position. It's used to detect if the user is over the menu bar

## z-index

So, z-index has been driving me crazy for a long time, so I decided to simplify [simplify the logic] by ordering all elements in an `order` array and then using `...zIndexFor(ELEMENTS.COMPOSE)` in the styles for the component that needs z-index. Smooth.
