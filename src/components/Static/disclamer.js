import React from 'react';

//styles
import * as S from './styles';

function Disclamer({}) {
  return (
    <S.Content>
    <div id="Description">
      <h3>Disclaimer</h3>
      <p>
        VideoPasal is affiliated with, associated to, and endorsed by OSR Digital. We respect their work and
        we build this app on our own. These changes can result in unexpected behaviour
        of our app, which we commit to keep up to date and working.
      </p>
    </div>
    </S.Content>
  );
}

export default Disclamer;
